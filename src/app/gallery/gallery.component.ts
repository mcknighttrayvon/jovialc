import { Component, OnInit } from "@angular/core";
import {NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation} from "ngx-gallery";

@Component({
  selector: "app-gallery",
  templateUrl: "./gallery.component.html",
  styleUrls: ["./gallery.component.css"]
})
export class GalleryComponent implements OnInit {
  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];

  constructor() {}

  ngOnInit() {
    this.galleryOptions = [
      {
        layout: "thumbnails-top",
        imageAutoPlay: true,
        imageAutoPlayPauseOnHover: true,
        previewAutoPlay: true,
        previewAutoPlayPauseOnHover: true
      },
      {
        breakpoint: 500,
        width: "300px",
        height: "300px",
        thumbnailsColumns: 3
      },
      { breakpoint: 300, width: "100%", height: "200px", thumbnailsColumns: 2 }
    ];

    this.galleryImages = [
      {
        small: "assets/images/events/carter_shower_cake.jpg",
        medium: "assets/images/events/carter_shower_cake.jpg",
        big: "assets/images/events/carter_shower_cake.jpg"
      },
      {
        small: "assets/images/events/carter_shower_treats.jpg",
        medium: "assets/images/events/carter_shower_treats.jpg",
        big: "assets/images/events/carter_shower_treats.jpg"
      },
      {
        small: "assets/images/events/coco1.jpg",
        medium: "assets/images/events/coco1.jpg",
        big: "assets/images/events/coco1.jpg"
      },
      {
        small: "assets/images/events/coco2.jpg",
        medium: "assets/images/events/coco2.jpg",
        big: "assets/images/events/coco2.jpg"
      },
      {
        small: "assets/images/events/grad.jpg",
        medium: "assets/images/events/grad.jpg",
        big: "assets/images/events/grad.jpg"
      },
      {
        small: "assets/images/events/h_unicorn_party.JPG",
        medium: "assets/images/events/h_unicorn_party.JPG",
        big: "assets/images/events/h_unicorn_party.JPG"
      },
      {
        small: "assets/images/events/merm1.jpeg",
        medium: "assets/images/events/merm1.jpeg",
        big: "assets/images/events/merm1.jpeg"
      },
      {
        small: "assets/images/events/merm2.jpeg",
        medium: "assets/images/events/merm2.jpeg",
        big: "assets/images/events/merm2.jpeg"
      }
    ];
  }
}
