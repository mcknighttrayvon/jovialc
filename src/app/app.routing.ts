import {RouterModule, Routes} from '@angular/router'
import {ModuleWithProviders} from '@angular/core'
import { HomeComponent } from './home/home.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ContactComponent } from './contact/contact.component';
//import { GalleryComponent } from './gallery/gallery.component';
import { ServicesComponent } from './services/services.component';
//import { TreatsComponent } from './treats/treats.component';
import { PromotionsComponent } from './promotions/promotions.component';

const routes: Routes = [{ path: 'home', component: HomeComponent },
                        { path: 'aboutus', component: AboutusComponent},
                        { path: 'services', component: ServicesComponent},
                        //{ path: 'treats', component: TreatsComponent},
                        //{ path: 'gallery', component: GalleryComponent},
                        { path: 'contact', component: ContactComponent},
                        { path: 'promotions', component: PromotionsComponent},
                        { path: '', component: HomeComponent}
                    ];

export const routingModule: ModuleWithProviders = RouterModule.forRoot(routes);

