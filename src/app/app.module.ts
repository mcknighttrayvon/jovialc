import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { routingModule} from './app.routing'
import { NgxGalleryModule } from 'ngx-gallery';

import { AppComponent } from "./app.component";
import { AboutusComponent } from './aboutus/aboutus.component';
import { ServicesComponent } from './services/services.component';
import { GalleryComponent } from './gallery/gallery.component';
import { ContactComponent } from './contact/contact.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { TreatsComponent } from './treats/treats.component';
import { PromotionsComponent } from './promotions/promotions.component';

@NgModule({
  declarations: [AppComponent, AboutusComponent, HomeComponent, ServicesComponent, GalleryComponent, ContactComponent, HeaderComponent, FooterComponent, HomeComponent, TreatsComponent, PromotionsComponent],
  imports: [BrowserModule, routingModule, NgxGalleryModule],
  providers: [],
  bootstrap: [AppComponent, AboutusComponent, HomeComponent, ServicesComponent, GalleryComponent, ContactComponent, HeaderComponent, FooterComponent, HomeComponent, TreatsComponent, PromotionsComponent]
})
export class AppModule {}

