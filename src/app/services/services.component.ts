import { Component, OnInit } from '@angular/core';
import {NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation} from "ngx-gallery";

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css']
})
export class ServicesComponent implements OnInit {
  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];

  constructor() { }

  ngOnInit() {

    this.galleryOptions = [
      {
        layout: "thumbnails-bottom",
        imageAutoPlay: true,
        imageAutoPlayPauseOnHover: true,
        previewAutoPlay: true,
        previewAutoPlayPauseOnHover: true,
        imageAnimation: NgxGalleryAnimation.Zoom,
        imageInfinityMove: true,
        imageAutoPlayInterval: 4000
      },
      {
        breakpoint: 500,
        width: "300px",
        height: "300px",
        thumbnailsColumns: 4
      },
      { breakpoint: 300, 
        width: "100%", 
        height: "200px", 
        thumbnailsColumns: 2 }
    ];

    this.galleryImages = [
      //CARTER BABY SHOWER
      {
        small: "assets/images/events/carter_shower_cake.jpg",
        medium: "assets/images/events/carter_shower_cake.jpg",
        big: "assets/images/events/carter_shower_cake.jpg"
      },
      {
        small: "assets/images/events/carter_shower_treats.jpg",
        medium: "assets/images/events/carter_shower_treats.jpg",
        big: "assets/images/events/carter_shower_treats.jpg"
      },
      //UNICORN PARTY
      {
        small: "assets/images/events/h_unicorn_party.JPG",
        medium: "assets/images/events/h_unicorn_party.JPG",
        big: "assets/images/events/h_unicorn_party.JPG"
      },
      //CARTER 1ST BDAY
      {
        small: "assets/images/events/1stBdayC.jpeg",
        medium: "assets/images/events/1stBdayC.jpeg",
        big: "assets/images/events/1stBdayC.jpeg"
      },
      {
        small: "assets/images/events/1stBdaySis.jpeg",
        medium: "assets/images/events/1stBdaySis.jpeg",
        big: "assets/images/events/1stBdaySis.jpeg"
      },
      {
        small: "assets/images/events/1stBdayTable.jpeg",
        medium: "assets/images/events/1stBdayTable.jpeg",
        big: "assets/images/events/1stBdayTable.jpeg"
      },
//      {
//        small: "assets/images/events/babyC.jpeg",
//        medium: "assets/images/events/babyC.jpeg",
//        big: "assets/images/events/babyC.jpeg"
//      },

      //STOCK PHOTOS
      {
        small: "assets/images/events/coco1.jpg",
        medium: "assets/images/events/coco1.jpg",
        big: "assets/images/events/coco1.jpg"
      },
      {
        small: "assets/images/events/coco2.jpg",
        medium: "assets/images/events/coco2.jpg",
        big: "assets/images/events/coco2.jpg"
      },
      {
        small: "assets/images/events/grad.jpg",
        medium: "assets/images/events/grad.jpg",
        big: "assets/images/events/grad.jpg"
      },
      {
        small: "assets/images/events/merm1.jpeg",
        medium: "assets/images/events/merm1.jpeg",
        big: "assets/images/events/merm1.jpeg"
      },
      {
        small: "assets/images/events/merm2.jpeg",
        medium: "assets/images/events/merm2.jpeg",
        big: "assets/images/events/merm2.jpeg"
      },

      //ANNIVERSARY
      {
        small: "assets/images/events/anniv_dance.jpg",
        medium: "assets/images/events/anniv_dance.jpg",
        big: "assets/images/events/anniv_dance.jpg"
      },
      {
        small: "assets/images/events/anniv_desserts.jpg",
        medium: "assets/images/events/anniv_desserts.jpg",
        big: "assets/images/events/anniv_desserts.jpg"
      },
      {
        small: "assets/images/events/anniv_desserts2.jpg",
        medium: "assets/images/events/anniv_desserts2.jpg",
        big: "assets/images/events/anniv_desserts2.jpg"
      },
      {
        small: "assets/images/events/anniv_tables.jpg",
        medium: "assets/images/events/anniv_tables.jpg",
        big: "assets/images/events/anniv_tables.jpg"
      },
      {
        small: "assets/images/events/anniv_vips.jpg",
        medium: "assets/images/events/anniv_vips.jpg",
        big: "assets/images/events/anniv-vips.jpg"
      },

      //CUSTOMS
      {
        small: "assets/images/events/bling&bless.jpg",
        medium: "assets/images/events/bling&bless.jpg",
        big: "assets/images/events/bling&bless.jpg"
      },
      {
        small: "assets/images/events/briggsCutout.jpg",
        medium: "assets/images/events/briggsCutout.jpg",
        big: "assets/images/events/briggsCutout.jpg"
      },
      {
        small: "assets/images/events/babyCake.jpg",
        medium: "assets/images/events/babyCake.jpg",
        big: "assets/images/events/babyCake.jpg"
      },
      {
        small: "assets/images/events/babyHi5.jpg",
        medium: "assets/images/events/babyHi5.jpg",
        big: "assets/images/events/babyHi5.jpg"
      }
    ];  
  }
}
