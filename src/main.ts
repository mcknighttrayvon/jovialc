import { enableProdMode } from "@angular/core";
import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";

import { AppModule } from "./app/app.module";
import { environment } from "./environments/environment";
import * as $ from "jquery";


if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic()
  .bootstrapModule(AppModule)
  .catch(err => console.error(err));

(function($) {
  "use strict";

  // PRE LOADER
  window.addEventListener(
    "load",
    function() {
      $(".preloader").fadeOut(1000); // set duration in brackets
    },
    false
  );

})(jQuery);

